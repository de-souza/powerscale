#
#    Copyright 2018 Léo De Souza
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
import argparse
import numpy as np


def powerscale(filename, start, end, num, exp):
    """Save a list of integers in power scale in a text file."""
    start_inverse = np.power(start, 1 / exp)
    end_inverse = np.power(end, 1 / exp)
    scale = np.rint(np.linspace(start_inverse, end_inverse, num) ** exp)
    np.savetxt(filename, scale, "%d")


def parse_args():
    """Argument parser."""
    parser = argparse.ArgumentParser(
        description="Save a list of integers in power scale in a text file."
    )
    parser.add_argument("-f", "--filename", default="scale.txt", help="saved file name")
    parser.add_argument("-s", "--start", default=103, type=float, help="starting value")
    parser.add_argument("-e", "--end", default=99999, type=float, help="ending value")
    parser.add_argument("-n", "--num", default=25, type=int, help="number of values")
    parser.add_argument("-p", "--exp", default=2.5, type=float, help="scale exponent")
    args = parser.parse_args()
    return args.filename, args.start, args.end, args.num, args.exp


if __name__ == "__main__":
    powerscale(*parse_args())
