from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="powerscale",
    version="0.1.0",
    description="Save a list of integers in power scale in a text file.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Léo De Souza",
    author_email="43355143+de-souza@users.noreply.github.com",
    url="https://gitlab.com/de-souza/powerscale",
    packages=find_packages(),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
)
